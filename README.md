# yamaha VKB-100 Vocaloid Bluetooth


- The midi and audio devices both work when you plug them in but the midi interface doesn't send data until you change a setting.

Hypothesis. 

- The mobile app is communicating over bluetooth sending commands to a midi keyboard. 
- The keyboard can be controled by midi
- It might also be controllable using undocumented midi commands to the usb midi interface.



## Turning midi keyboard on and off


```
Opcode: Write Command (0x52)
    0... .... = Authentication Signature: False
    .1.. .... = Command: True
    ..01 0010 = Method: Write Request (0x12)
```


| control | value |
|--|--|
| on | 8080f0437301522527100000110180f7|
| off | 8080f0437301522527100000110080f7|

## midi tx channel

| control | value |
|--|--|
| 1 | 8080f0437301522527100000100080f7 |
| 2 | 8080f0437301522527100000100180f7 |
| 3 | 8080f0437301522527100000100280f7 |
| 4 | 8080f0437301522527100000100380f7 |
| 5 | 8080f0437301522527100000100480f7 |
| 6 | 8080f0437301522527100000100580f7 |
| 7 | 8080f0437301522527100000100680f7 |
| 8 | 8080f0437301522527100000100780f7 |
| 9 | 8080f0437301522527100000100880f7 |
| 10 | 8080f0437301522527100000100980f7 |
| 11 | 8080f0437301522527100000100a80f7 |
| 12 | 8080f0437301522527100000100b80f7 |
| 13 | 8080f0437301522527100000100c80f7 |
| 14 | 8080f0437301522527100000100d80f7 |
| 15 | 8080f0437301522527100000100e80f7 |
| 16 | 8080f0437301522527100000100f80f7 |

## Internal Speaker on / off


| control | value |
|--|--|
| on | 8080f0437301522527100000120180f7 |
| off | 8080f0437301522527100000120080f7|


## 

## Equalizer


| control | value |
|--|--|
| Flat | 8080f0437301522527100000050080f7  |
| Boost | 8080f0437301522527100000050180f7  |
| Bright | 8080f0437301522527100000050280f7 |
| Mild | 8080f0437301522527100000050380f7 |
